# home_website

[craigdutton.me](https://craigdutton.me)

## Summary

A small website to experiement and portfolio some Devops skills. The website is deployed automatically via a terraform script.

## What does Terraform do?

1. Creates and configures an S3 Bucket for static website hosting.
2. Creates a new Certificate in ACM
3. Sets up a CloudFront distribution to host the website on my domain with HTTPS.

## Technologies

- Terraform, for the Infrastructure as Code deployment
- GitLab for source control
- AWS S3 and CloudFront for the serverless infrastructure
- HTML/CSS for the web pages

## Depencencies

https://github.com/kevquirk/simple.css
A fantastic and simple css library
