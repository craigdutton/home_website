terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "ap-southeast-2"
}

provider "aws" {
  region = "us-east-1"
  alias  = "us"
}

variable "site_domain" {
  type        = string
  description = "The domain name to use for the static site"
  default = "craigdutton.me"
}

# Create the bucket
resource "aws_s3_bucket" "bucket" {
  bucket = "craigdutton.me"
}

# Give the bucket a website
resource "aws_s3_bucket_website_configuration" "bucket_website" {
  bucket = aws_s3_bucket.bucket.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

# Permissions - ACL
resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id

  acl = "public-read"
}

# Permissions - policy
resource "aws_s3_bucket_policy" "site" {
  bucket = aws_s3_bucket.bucket.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:GetObject"
        Resource = [
          aws_s3_bucket.bucket.arn,
          "${aws_s3_bucket.bucket.arn}/*",
        ]
      },
    ]
  })
}

# Files to be uploaded
variable "files" {
  type = map(object({
    name = string
    source = string
    type = string
  }))
  default = {
    "index" = {
      name = "index.html"
      source = "html/index.html"
      type = "text/html"
    },
    "site" = {
      name = "site.html"
      source = "html/site.html"
      type = "text/html"
    },
    "error" = {
      name = "error.html"
      source = "html/error.html"
      type = "text/html"
    },
    "robots" = {
      name = "robots.txt"
      source = "robots.txt"
      type = "text/plain"
    },
    "gitlab" = {
      name = "images/gitlab.png"
      source = "images/gitlab.png"
      type = "binary/octet-stream"
    },
    "linkedin" = {
      name = "images/linkedin.png"
      source = "images/linkedin.png"
      type = "binary/octet-stream"
    },
    "favicon" = {
      name = "favicon.ico"
      source = "images/favicon.ico"
      type = "binary/octet-stream"
    },
    "css" = {
      name = "css/simple-min.css"
      source = "css/simple-min.css"
      type = "text/css"
    }
  }
}

# Upload files
resource "aws_s3_object" "upload" {
  for_each = var.files

  bucket = "craigdutton.me"
  key    = each.value.name
  source = each.value.source
  etag = filemd5(each.value.source)
  content_type = each.value.type
}

# Request Certificate
resource "aws_acm_certificate" "cert" {
  provider          = aws.us
  domain_name       = var.site_domain
  subject_alternative_names = ["www.${var.site_domain}"]
  validation_method = "DNS"

  tags = {
    Name = var.site_domain
  }
}

# Validate Certificate
resource "aws_acm_certificate_validation" "cert" {
  provider        = aws.us
  certificate_arn = aws_acm_certificate.cert.arn
}

# Cloudfront CDN - necessary for HTTPS
resource "aws_cloudfront_distribution" "cdn" {
  origin {
    domain_name = aws_s3_bucket_website_configuration.bucket_website.website_endpoint
    origin_id   = aws_s3_bucket.bucket.id
    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }
  enabled             = true
  default_root_object = "index.html"

  aliases = [var.site_domain, "www.${var.site_domain}"]

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.bucket.id
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 600
    max_ttl                = 3600
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate_validation.cert.certificate_arn
    ssl_support_method  = "sni-only"
  }

  custom_error_response {
    error_code = 404
    response_code = 200
    response_page_path = "/error.html"
  }
}
