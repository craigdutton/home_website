output "website_bucket_name" {
  description = "Name (id) of the bucket"
  value       = aws_s3_bucket.bucket.id
}

output "bucket_endpoint" {
  description = "Bucket endpoint"
  value       = aws_s3_bucket.bucket.website_endpoint
}
